<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application main page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        return view('main', ['questions' => $questions]);
    }


    public function showQuestion($id)
    {
        $question = Question::find($id);
        $answers = $question ? $question->answers : [];
        $ratings = Auth::check() ? Rating::checkRating($answers) : [];
        return view('question', ['question' => $question, 'answers' => $answers, 'ratings' => $ratings]);
    }

    public function createQuestion()
    {
        return view('create');
    }

    public function store(Request $request) {
        if (Auth::check()) {

            $this->validate($request, [
                'content' => 'required',
            ]);

            $question = new Question($request->all());
            $question->user_id = Auth::id();
            $question->save();

            return redirect(action('HomeController@showQuestion', ['id' => $question->id]));
        }
        else {
            return redirect(route('login'));
        }
    }
}
