<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rating;

class AnswerController extends Controller
{
    public function store(Request $request) {
        if (Auth::check()) {

            $this->validate($request, [
                'content' => 'required',
            ]);

            $answer = new Answer($request->all());
            $answer->user_id = Auth::id();
            $answer->save();

            return redirect(action('HomeController@showQuestion', ['id' => $answer->question_id]));
        }
        else {
            return redirect(route('login'));
        }
    }


    public function rating(Request $request)
    {
        if (Auth::check()) {
            $userID = Auth::id();
            $rating = Rating::where([
                ['user_id', '=', $userID],
                ['answer_id', '=', $request->input('answer_id')],
            ])->first();
            if (!$rating) {
                $rating = new Rating($request->all());
                $rating->user_id = $userID;
            }
            else {
                if ($rating->rating == $request->input('rating')) {
                    return redirect(action('HomeController@showQuestion', ['id' => $answer->question_id]));
                }
                $rating->rating += $request->input('rating');
            }
            $rating->save();
            $answer = Answer::find($request->input('answer_id'));
            $answer->rating += $request->input('rating');
            $answer->save();

            return redirect(action('HomeController@showQuestion', ['id' => $answer->question_id]));
        }
        else {
            return redirect(route('login'));
        }
    }

}
