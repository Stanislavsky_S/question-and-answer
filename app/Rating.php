<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $table = 'rating';

    protected $fillable = [
        'answer_id', 'rating'
    ];

    public static function checkRating($answers)
    {
        if (count($answers)) {
            $result = [];
            $userID = Auth::id();
            foreach ($answers as $a) {
                $temp = self::where([
                    ['user_id', '=', $userID],
                    ['answer_id', '=', $a->id],
                ])->first();
                $result[$a->id] = $temp ? $temp->rating : 0;
            }

            return $result;
        }
        else {
            return [];
        }
    }
}
