<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/question/{id}', 'HomeController@showQuestion');
Route::get('/create', 'HomeController@createQuestion')->middleware('auth');
Route::post('/store_question', 'HomeController@store')->middleware('auth');
Route::post('/store_answer', 'AnswerController@store')->middleware('auth');
Route::post('/rating', 'AnswerController@rating')->name('rating')->middleware('auth');

