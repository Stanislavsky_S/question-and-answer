@extends('layouts.app')

@section('title', 'Главная')

@section('content')
<div class="container">
    @if (count($questions))
        <div class="row text-center">
            <h2>Вопросы</h2>
        </div>
        @foreach($questions as $q)
            <div class="row text-center">
                <div class="panel panel-default">
                    <div class="panel-heading">Пользователь <span class="badge">{{ $q->user->name }}</span></div>
                    <div class="panel-body"><a href="{{action('HomeController@showQuestion', ['id' => $q->id])}}">
                            {{$q->content}}</a></div>
                </div>
            </div>
        @endforeach
    @else
        <div class="row text-center">
            <h2>Нет вопросов</h2>
        </div>
    @endif
</div>
@endsection