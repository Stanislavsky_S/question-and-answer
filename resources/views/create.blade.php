@extends('layouts.app')

@section('title', 'Новый вопрос')

@section('content')
    <div class="container">
        <h2>Новый вопрос</h2>
        @if (Auth::check())
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(['action' => 'HomeController@store']) !!}
                <div class="form-group">
                    {!! Form::label('content', 'Вопрос') !!}
                    {!! Form::textarea('content', '', ['class' => 'form-control']); !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Сохранить') !!}
                </div>
            {!! Form::close() !!}

        @else
        <div class="row">
            <div class="panel panel-warning">
                <div class="panel-body">Вы не вошли в систему</div>
            </div>
        </div>
        @endif
    </div>
@endsection