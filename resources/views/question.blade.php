@extends('layouts.app')

@section('title', 'Вопрос и ответы')

@section('content')
    <div class="container">
        @if ($question)
            <div class="row text-center">
                <h2>Вопрос</h2>
            </div>
            <div class="row text-left">
                <div class="panel panel-primary">
                    <div class="panel-heading">Пользователь <span class="badge">{{ $question->user->name }}</span></div>
                    <div class="panel-body">{{$question->content}}</div>
                </div>
            </div>

            <div class="row text-center">
                <h2>Ответы</h2>
            </div>

            @if ($answers)
                @foreach($answers as $a)
                    <div class="row text-left">
                        <div class="panel panel-info">
                            <div class="panel-heading">Пользователь <span class="badge">{{ $a->user->name }}</span>
                                <div style="float: right">
                                    @if (Auth::check())
                                        <button class="btn btn-danger btn-xs"
                                                @if ($ratings[$a->id] == -1) disabled @endif onclick="rate(-1, '{{$a->id}}')">-</button>
                                        <span class="badge">{{ $a->rating }}</span>
                                        <button class="btn btn-success btn-xs"
                                                @if ($ratings[$a->id] == 1) disabled @endif onclick="rate(1, '{{$a->id}}')">+</button>

                                        <form id="rating-form-{{$a->id}}" action="{{ route('rating') }}" method="POST" class="hidden">
                                            {!! Form::hidden('rating'); !!}
                                            {!! Form::hidden('answer_id', $a->id); !!}
                                            {{ csrf_field() }}
                                        </form>
                                    @else
                                        <span class="badge">{{ $a->rating }}</span>
                                    @endif
                                </div></div>
                            <div class="panel-body">{{$a->content}}</div>
                        </div>
                    </div>
                @endforeach
            @else

            @endif

            @if (Auth::check())
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(['action' => 'AnswerController@store']) !!}
                    <div class="form-group">
                        {!! Form::label('content', 'Ответ') !!}
                        {!! Form::textarea('content', '', ['class' => 'form-control']); !!}
                    </div>

                    {!! Form::hidden('question_id', $question->id); !!}

                    <div class="form-group">
                        {!! Form::submit('Сохранить') !!}
                    </div>
                {!! Form::close() !!}
            @endif

        @else
            <div class="row text-center">
                <h2>Нет вопроса</h2>
            </div>
        @endif

    </div>

    <script src="{{ asset('js/question.js') }}"></script>

@endsection