## Установка

Сделано на фрейморке Laravel 5.5.
Сервер должен соответствовать следующим требованием:
- PHP >= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

Также должен быть установлен Composer. 
Установка:
1. Копируем репозиторий к себе.
2. Настраиваем точку входа для сервера bank/public/index.php
3. Запускаем консоль, заходим в папку проекта и выполняем команду php composer update
4. Создаем в корне проекта файл .env по примеру файла .env.example, где прописываем настройки доступа к базе данных
5. Выполняем в консоли из корня проекта команду php artisan migrate