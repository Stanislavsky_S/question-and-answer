function rate(value, id) {
    event.preventDefault();
    var form = document.getElementById('rating-form-' + id);
    var ratingInput = form.querySelector('input:first-child');
    if (ratingInput) {
        ratingInput.value = value;
        form.submit();
    }
}